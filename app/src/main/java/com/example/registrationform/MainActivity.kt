package com.example.registrationform

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.Button
import android.widget.EditText
import androidx.core.view.ViewCompat
import java.lang.Integer.parseInt
import java.util.*
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity() {

    lateinit var saveButton: Button
    lateinit var clearButton: Button
    lateinit var username: EditText
    lateinit var age: EditText
    lateinit var email: EditText
    lateinit var firstName: EditText
    lateinit var lastName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        saveButton = findViewById(R.id.save)
        clearButton = findViewById(R.id.clear)
        username = findViewById(R.id.username)
        email = findViewById(R.id.email)
        firstName = findViewById(R.id.first_name)
        lastName = findViewById(R.id.last_name)
        age = findViewById(R.id.age)

        saveButton.setOnClickListener {
            saveAction(it)
        }
        clearButton.setOnLongClickListener {
            clearAction()
        }

    }

    private fun saveAction(view : View) {

        /**
        lateinit var myToast : Toast
        myToast.setGravity(Gravity.CENTER, 0, 0)
         */
        if (username.text.toString().isBlank() || email.text.toString().isBlank() ||
            firstName.text.toString().isBlank() || lastName.text.toString().isBlank() ||
            age.text.toString().isBlank()) {
                
                if (email.text.toString().isBlank()) email.error = "Must be filled"
                if (username.text.toString().isBlank()) username.error = "Must be filled"
                if (firstName.text.toString().isBlank()) firstName.error = "Must be filled"
                if (lastName.text.toString().isBlank()) lastName.error = "Must be filled"
                if (age.text.toString().isBlank()) age.error = "Must be filled"

            Toast.makeText(this, "All fields must be filled", Toast.LENGTH_SHORT).show()
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            email.error = "Invalid email address"
            Toast.makeText(this, "Please enter valid e-mail address", Toast.LENGTH_SHORT).show()
        }
        else if (username.text.toString().length < 10) {
            username.error = "At least 10 characters is MUST"
            Toast.makeText(this, "Username must contain no less than 10 characters", Toast.LENGTH_SHORT).show()
        }
        else if (parseInt(age.text.toString()) <= 0) {
            age.error = "Invalid age"
            Toast.makeText(this, "Please enter valid age", Toast.LENGTH_SHORT).show()
        }
        else {
            setContentView(R.layout.secondlayout)
            Timer("", false).schedule(3000) {
                val intent = intent
                finish()
                startActivity(intent)
            }
        }
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun clearAction(): Boolean {
        email.text.clear()
        username.text.clear()
        firstName.text.clear()
        lastName.text.clear()
        age.text.clear()

        return true
    }

}
